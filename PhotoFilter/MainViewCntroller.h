//
//  MainViewCntroller.h
//  PhotoFilter
//
//  Created by Maksim Rakhleev on 15.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewCntroller : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;

@property (strong, nonatomic) UIView *overlayView;

@end
