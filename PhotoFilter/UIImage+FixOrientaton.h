//
//  UIImage+FixOrientaton.h
//  PhotoFilter
//
//  Created by Maksim Rakhleev on 17.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FixOrientaton)

- (UIImage *)fixOrientation;

@end
