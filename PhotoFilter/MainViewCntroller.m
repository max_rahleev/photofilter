//
//  MainViewCntroller.m
//  PhotoFilter
//
//  Created by Maksim Rakhleev on 15.12.14.
//  Copyright (c) 2014 Maksim Rakhleev. All rights reserved.
//

#import "MainViewCntroller.h"
#import "ImageFilter.h"
#import "UIImage+FixOrientaton.h"

@interface MainViewCntroller () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (strong,nonatomic) UIImagePickerController *imagePicker;
@end

@implementation MainViewCntroller

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    [self.imagePicker setDelegate:self];
    [self.imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self.imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    [self.imagePicker setAllowsEditing:YES];
    [self.imagePicker setShowsCameraControls:NO];
    [self.imagePicker setNavigationBarHidden:YES];
    [self.imagePicker setToolbarHidden:YES];
    
    self.overlayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    self.overlayView.opaque = NO;
    self.overlayView.backgroundColor = [UIColor clearColor];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.overlayView.frame) - 44,
                                                                    CGRectGetWidth(self.overlayView.frame), 44)];
    [toolbar setBarStyle:UIBarStyleBlack];
    toolbar.items = [NSArray arrayWithObjects:
                     [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(actionDone:)],
                     [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil],
                     [[UIBarButtonItem alloc]initWithTitle:@"Switch" style:UIBarButtonItemStylePlain target:self action:@selector(swithCamera:)],
                     [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil],
                     [[UIBarButtonItem alloc]initWithTitle:@"Take a photo" style:UIBarButtonItemStylePlain target:self action:@selector(actionTakePhoto:)], nil];
    
    UIView *cameraView=[[UIView alloc] initWithFrame:self.view.bounds];
    [cameraView addSubview:self.overlayView];
    [cameraView addSubview:toolbar];
    
    self.imagePicker.cameraOverlayView = cameraView;
    
    [[self navigationItem] setRightBarButtonItem:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                                                                              target:self
                                                                                              action:@selector(takePicture:)]];
}


#pragma mark - <UIImagePickerControllerDelegate>

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    image = image.fixOrientation;
    
//    if (self.imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceFront) {
//        image = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];
//    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.photoImageView setImage:[image blueMood]];
    });
    
//    [self.photoImageView setImage:image];
    
    [self actionDone:nil];
}

/*
 
 - (IBAction)filter:(UIButton *)__unused sender {
 UIImage *image = [self.originalImage copy];
 dispatch_async(dispatch_get_main_queue(), ^{
 self.imageView.image = [image blueMood];
 });
 }
 
 - (IBAction)revert:(UIButton *)__unused sender {
 self.imageView.image = self.originalImage;
 self.imageView.image.filter = nil;
 }
 
 */

#pragma mark - Actions

-(void)takePicture:(id)sender {
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void)actionDone:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionTakePhoto:(UIBarButtonItem *)sender {
    [self.imagePicker takePicture];
}

- (void)swithCamera:(UIBarButtonItem *)sender {
    if (self.imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceFront) {
        [self.imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    } else {
        [self.imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }
}

@end
